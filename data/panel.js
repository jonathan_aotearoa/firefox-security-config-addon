const checkboxes = document.querySelectorAll("form input[type='checkbox']");

for (i = 0; i < checkboxes.length; i++) {
  checkboxes[i].addEventListener("change", changeHandler);
}

function changeHandler(evt) {
  const id = evt.target.id;
  const checked = evt.target.checked;
  const payload = {"id": id, "checked": checked};
  self.port.emit("change", payload);
}

self.port.on("prefs", function(prefs) {
  for (i = 0; i < checkboxes.length; i++) {
    const checkbox = checkboxes[i];
    checkbox.checked = prefs[checkbox.id];
  }
});

self.port.on("get-resize-dimensions", function() {
  const content = document.querySelector("#content");
  self.port.emit("resize-dimensions", {"width": content.offsetWidth, "height": content.offsetHeight + 20});
});
