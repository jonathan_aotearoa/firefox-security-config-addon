const { ToggleButton } = require('sdk/ui/button/toggle');
const panels = require("sdk/panel");
const self = require("sdk/self");

const prefs = require("./lib/prefs.js");

const button = ToggleButton({
  id: "settings-button",
  label: "Security Configuration",
  icon: {
    "16": "./icon-16.png",
    "32": "./icon-32.png",
    "64": "./icon-64.png"
  },
  onChange: handleChange
});

const panel = panels.Panel({
  width: 280,
  contentURL: self.data.url("panel.html"),
  contentScriptFile: self.data.url("panel.js"),
  onShow: handleShow,
  onHide: handleHide
});

panel.port.on("change", function(payload) {
  if (payload.id === "do-not-track") {
    prefs.doNotTrack.set(payload.checked);
  } else if (payload.id === "disable-caches") {
    prefs.caches.set(!payload.checked);
  } else if (payload.id === "disable-clipboard-events") {
    prefs.clipboardEvents.set(!payload.checked);
  } else if (payload.id === "disable-geolocation") {
    prefs.geolocation.set(!payload.checked);
  } else if (payload.id === "disable-web-rtc") {
    prefs.webRtc.set(!payload.checked);
  } else if (payload.id === "disable-ciphers") {
    prefs.weakCiphers.set(!payload.checked);
  } else if (payload.id === "require-safe-ssl-negotiation") {
    prefs.sslSafeNegotiation.set(payload.checked);
  }
});

panel.port.on("resize-dimensions", function(dimensions) {
  panel.resize(dimensions.width, dimensions.height);
});

function handleShow() {
  panel.port.emit("get-resize-dimensions");
}

function handleHide() {
  button.state('window', {checked: false});
}

function handleChange(state) {
  if (state.checked) {
    const prefs = getPrefs();
    panel.port.emit("prefs", prefs);
    panel.show({
      position: button
    });
  }
}

function getPrefs() {
  return {
    "do-not-track": prefs.doNotTrack.is(true),
    "disable-caches": prefs.caches.is(false),
    "disable-clipboard-events": prefs.clipboardEvents.is(false),
    "disable-geolocation": prefs.geolocation.is(false),
    "disable-web-rtc": prefs.webRtc.is(false),
    "disable-ciphers": prefs.weakCiphers.is(false),
    "require-safe-ssl-negotiation": prefs.sslSafeNegotiation.is(true)
  };
}