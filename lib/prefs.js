const BooleanPreferences = require("./boolean-prefs.js");

const caches = new BooleanPreferences(['browser.cache.disk.enable', 'browser.cache.disk_cache_ssl', 'browser.cache.offline.enable']);
const weakCiphers = new BooleanPreferences(["security.ssl3.ecdhe_ecdsa_rc4_128_sha", "security.ssl3.ecdhe_rsa_rc4_128_sha", "security.ssl3.rsa_rc4_128_md5", "security.ssl3.rsa_rc4_128_sha", "security.ssl3.rsa_des_ede3_sha"]);
const clipboardEvents = new BooleanPreferences(["dom.event.clipboardevents.enabled"]);
const doNotTrack = new BooleanPreferences(["privacy.donottrackheader.enabled"]);
const geolocation = new BooleanPreferences(["geo.enabled"]);
const sslSafeNegotiation = new BooleanPreferences(["security.ssl.require_safe_negotiation", "security.ssl.treat_unsafe_negotiation_as_broken"]);
const webRtc = new BooleanPreferences(["media.peerconnection.enabled"]);

module.exports = {
    "caches": caches,
    "weakCiphers": weakCiphers,
    "clipboardEvents": clipboardEvents,
    "doNotTrack": doNotTrack,
    "geolocation": geolocation,
    "sslSafeNegotiation": sslSafeNegotiation,
    "webRtc": webRtc
};