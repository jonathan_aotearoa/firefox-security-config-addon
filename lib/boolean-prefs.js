const preferencesService = require("sdk/preferences/service");

/**
 * Creates a new instance
 *
 * @param preferenceNames
 * @constructor
 */
function BooleanPreferences(preferenceNames) {
    this.preferenceNames = preferenceNames;
}

BooleanPreferences.prototype.set = function (value) {
    if (typeof value !== "boolean") {
        throw new TypeError("value argument must be a boolean");
    }
    for (i = 0; i < this.preferenceNames.length; i++) {
        const preferenceName = this.preferenceNames[i];
        preferencesService.set(preferenceName, value);
    }
};

BooleanPreferences.prototype.is = function (value) {
    if (typeof value !== "boolean") {
        throw new TypeError("value argument must be a boolean");
    }
    for (i = 0; i < this.preferenceNames.length; i++) {
        const preferenceName = this.preferenceNames[i];
        const actualValue = preferencesService.get(preferenceName, false);
        if (actualValue !== value) return false;
    }
    return true;
};

module.exports = BooleanPreferences;