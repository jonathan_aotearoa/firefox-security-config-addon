# Firefox Security Configuration Add-on

## Overview

The aim of this add-on is to simplify the task of hardening Firefox by providing a user interface for enabling/disabling single and/or groups of security-related properties. The majority of these properties can only be modified via about:config, which is not a user friendly option.

![Screenshot 1](https://bitbucket.org/jonathan_aotearoa/firefox-security-config-addon/downloads/screenshot1.png)

## Development Environment

* Running on Ubuntu: ``jpm run -b /usr/bin/firefox``
* Running unit tests on Ubuntu: ``jpm --verbose test -b /usr/bin/firefox``

## Configuration Properties

### Enable Do Not Track (DNT) Header

Sets the following property to true:

* ``privacy.donottrackheader.enabled``

### Disable Caches

Set the following properties to false:

* ``browser.cache.disk.enable``
* ``browser.cache.disk_cache_ssl``
* ``browser.cache.offline.enable``

### Disable Clipboard Events

Sets the following property to false:

* ``dom.event.clipboardevents.enabled``

### Disable Geolocation

Sets the following property to false:

* ``geo.enabled``

### Disable Web RTC

Sets the following property to false:

* ``media.peerconnection.enabled``

### Disable Weak Ciphers

Sets the following properties to false:

* ``security.ssl3.ecdhe_ecdsa_rc4_128_sha``
* ``security.ssl3.ecdhe_rsa_rc4_128_sha``
* ``security.ssl3.rsa_rc4_128_md5``
* ``security.ssl3.rsa_rc4_128_sha``
* ``security.ssl3.rsa_des_ede3_sha``

### Require Safe SSL Negotiation

Sets the following properties to true:

* ``security.ssl.require_safe_negotiation``
* ``security.ssl.treat_unsafe_negotiation_as_broken``


