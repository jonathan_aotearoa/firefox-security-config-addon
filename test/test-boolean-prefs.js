const BooleanPreferences = require("../lib/boolean-prefs.js");

exports["test passing non boolean argument to set method"] = function (assert) {
    const booleanPrefs = new BooleanPreferences(["pref-name-1", "pref-name-2"]);
    assert.throws(function () {
        booleanPrefs.set("foo");
    }, "set method boolean argument type check works");
};

exports["test passing non boolean argument to is method"] = function (assert) {
    const booleanPrefs = new BooleanPreferences(["pref-name-1", "pref-name-2"]);
    assert.throws(function () {
        booleanPrefs.is("foo");
    }, "set method boolean argument type check works");
};

require("sdk/test").run(exports);